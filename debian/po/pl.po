# Copyright (C) 2006
# This file is distributed under the same license as the phpldapadmin package.
#
# Michał Kułach <michal.kulach@gmail.com>, 2012.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: phpldapadmin@packages.debian.org\n"
"POT-Creation-Date: 2012-02-06 12:20+0100\n"
"PO-Revision-Date: 2012-02-11 13:36+0100\n"
"Last-Translator: Michał Kułach <michal.kulach@gmail.com>\n"
"Language-Team: Polish <debian-l10n-polish@lists.debian.org>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.2\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"

#. Type: string
#. Description
#: ../phpldapadmin.templates:1001
msgid "LDAP server host address:"
msgstr "Adres serwera LDAP:"

#. Type: string
#. Description
#: ../phpldapadmin.templates:1001
msgid ""
"Please enter the host name or the address of the LDAP server you want to "
"connect to."
msgstr ""
"Proszę wprowadzić nazwę komputera lub adres serwera LDAP, z którym ma "
"nastąpić połączenie."

#. Type: boolean
#. Description
#: ../phpldapadmin.templates:2001
msgid "Enable support for ldaps protocol?"
msgstr "Włączyć obsługę protokołu ldaps?"

#. Type: boolean
#. Description
#: ../phpldapadmin.templates:2001
msgid ""
"If your LDAP server supports TLS (Transport Security Layer), you can use the "
"ldaps protocol to connect to it."
msgstr ""
"Jeśli bieżący serwer LDAP obsługuje TLS (Transport Security Layer), można "
"użyć protokołu ldaps do połączenia z nim."

#. Type: string
#. Description
#: ../phpldapadmin.templates:3001
msgid "Distinguished name of the search base:"
msgstr "Unikalna nazwa bazy wyszukiwania:"

#. Type: string
#. Description
#: ../phpldapadmin.templates:3001
msgid ""
"Please enter the distinguished name of the LDAP search base. Many sites use "
"the components of their domain names for this purpose. For example, the "
"domain \"example.com\" would use \"dc=example,dc=com\" as the distinguished "
"name of the search base."
msgstr ""
"Proszę wprowadzić unikalną nazwę (ang. distinguished name) jako podstawę "
"wyszukiwania LDAP. Wiele stron używa w tym celu części swoich nazw "
"domenowych. Przykładowo, domena \"example.com\" może używać \"dc=example,"
"dc=com\" jako unikalną nazwę podstawy wyszukiwania."

#. Type: select
#. Choices
#: ../phpldapadmin.templates:4001
msgid "session"
msgstr "sesja"

#. Type: select
#. Choices
#: ../phpldapadmin.templates:4001
msgid "cookie"
msgstr "ciasteczko"

#. Type: select
#. Choices
#: ../phpldapadmin.templates:4001
msgid "config"
msgstr "konfigur."

#. Type: select
#. Description
#: ../phpldapadmin.templates:4002
msgid "Type of authentication"
msgstr "Typ uwierzytelniania"

#. Type: select
#. Description
#: ../phpldapadmin.templates:4002
msgid ""
"session : You will be prompted for a login dn and a password everytime\n"
"          you connect to phpLDAPadmin, and a session variable on the\n"
"          web server will store them. It is more secure so this is the\n"
"          default."
msgstr ""
"sesja:      Użytkownik będzie pytany o login dn i hasło za każdym razem\n"
"            gdy będzie się łączył z phpLDAPadmin, a zmienna sesji na\n"
"            serwerze WWW będzie je przechowywać. Jest to bezpieczniejsze\n"
"            rozwiązanie, dlatego też jest ustawieniem domyślnym."

#. Type: select
#. Description
#: ../phpldapadmin.templates:4002
msgid ""
"cookie :  You will be prompted for a login dn and a password everytime\n"
"          you connect to phpLDAPadmin, and a cookie on your client will\n"
"          store them."
msgstr ""
"ciasteczko: Użytkownik będzie pytany o login dn i hasło za każdym razem,\n"
"            gdy będzie się łączył z phpLDAPadmin, a ciasteczko klienta\n"
"            będzie je przechowywać."

#. Type: select
#. Description
#: ../phpldapadmin.templates:4002
msgid ""
"config  : login dn and password are stored in the configuration file,\n"
"          so you have not to specify them when you connect to\n"
"          phpLDAPadmin."
msgstr ""
"konfigur.:  Login dn i hasło są przechowywane w pliku konfiguracyjnym,\n"
"            tak więc nie ma potrzeby ich podawania podczas łączenia z\n"
"            phpLDAPadmin."

#. Type: string
#. Description
#: ../phpldapadmin.templates:5001
msgid "Login dn for the LDAP server:"
msgstr "Login dn do serwera LDAP:"

#. Type: string
#. Description
#: ../phpldapadmin.templates:5001
msgid ""
"Enter the name of the account that will be used to log in to the LDAP "
"server. If you chose a form based authentication this will be the default "
"login dn. In this case you can also leave it empty, if you do  not want a "
"default one."
msgstr ""
"Proszę wprowadzić nazwę konta, które będzie używane do logowania się do "
"serwera LDAP. Jeśli wybrano uwierzytelnianie przez formularz, będzie to "
"domyślny login dn. W takim przypadku można także pozostawić to pole puste, "
"aby nie ustawiać domyślnego loginu."

#. Type: string
#. Description
#: ../phpldapadmin.templates:6001
msgid "Login password for the LDAP server:"
msgstr "Hasło loginu do serwera LDAP:"

#. Type: string
#. Description
#: ../phpldapadmin.templates:6001
msgid ""
"Enter the password that will be used to log in to the LDAP server. Note: the "
"password will be stored in clear text in config.php, which is not world-"
"readable."
msgstr ""
"Proszę wprowadzić hasło, które będzie używane do logowania się do serwera "
"LDAP. Uwaga: hasło będzie przechowywane jako czysty tekst w pliku config."
"php; nie jest on dostępny do odczytu dla innych."

#. Type: multiselect
#. Choices
#: ../phpldapadmin.templates:7001
msgid "apache2"
msgstr "apache2"

#. Type: multiselect
#. Description
#: ../phpldapadmin.templates:7002
msgid "Web server(s) which will be reconfigured automatically:"
msgstr ""
"Serwer(y) WWW, które będą ponownie skonfigurowane w sposób automatyczny:"

#. Type: multiselect
#. Description
#: ../phpldapadmin.templates:7002
msgid ""
"phpLDAPadmin supports any web server that PHP does, but this automatic "
"configuration process only supports Apache2."
msgstr ""
"phpLDAPadmin obsługuje dowolny serwer WWW, który obsługuje także PHP, ale "
"automatyczny proces konfiguracji dostępny jest tylko dla Apache2."

#. Type: boolean
#. Description
#: ../phpldapadmin.templates:8001
msgid "Should your webserver(s) be restarted?"
msgstr "Czy serwer(y) WWW powinny zostać zrestartowane?"

#. Type: boolean
#. Description
#: ../phpldapadmin.templates:8001
msgid ""
"Remember that in order to apply the changes your webserver(s) has/have to be "
"restarted."
msgstr ""
"Proszę pamiętać, że w celu wprowadzenia zmian, konieczne jest zrestartowanie "
"serwera (serwerów) WWW."

#~ msgid "session, cookie, config"
#~ msgstr "sesja, ciasteczko, konfiguracja"

#~ msgid "apache, apache-ssl, apache-perl, apache2"
#~ msgstr "apache, apache-ssl, apache-perl, apache2"

#~ msgid "Restart of your webserver(s)"
#~ msgstr "Proszę zrestartować serwer(y) WWW"
